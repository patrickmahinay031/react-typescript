import React, { FormEvent, useState } from 'react'
import { postLoginApiData } from './Api';

const Login = () => {

    interface credential {
        email?: string
        password?: string
    }

    const [loginCredential, setLoginCredential] = useState<credential | {}>({});

    const loginAction = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        const params: credential = {
            ...loginCredential
        };

        await postLoginApiData("/login", params).then((res: any) => {
            console.log(res)
        }).catch((err: any) => {
            console.log(err)
        })

    }

    return (
        <div className='flex justify-center items-center min-h-screen bg-purple-900'>
            <form onSubmit={loginAction} className='flex flex-col space-y-3'>
                <div>
                    <label className='p-1 text-white font-bold drop-shadow-sm'>Username</label>
                    <input
                        id="emailInput"
                        className='p-1 rounded'
                        name="email"
                        type="email"
                        placeholder="Username"
                        onChange={(e) => { setLoginCredential({ ...loginCredential, email: e.target.value }) }}
                        required
                    />
                </div>
                <div>
                    <label className='p-1 text-white font-bold drop-shadow-sm'>Password</label>
                    <input
                        id="passwordInput"
                        className='p-1 rounded'
                        name="password"
                        type="password"
                        placeholder="Password"
                        onChange={(e) => { setLoginCredential({ ...loginCredential, password: e.target.value }) }}
                        required
                    />
                </div>

                <input
                    className='bg-blue-500 text-white font-bold p-1 rounded'
                    type="submit"
                >
                </input>
            </form>
        </div>
    )
}

export default Login
