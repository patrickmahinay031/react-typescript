
import api from './Server';

export const postLoginApiData = (routeName: string, params: object): any => {
    return api.post(routeName, params).then((res: any) => { return res })
    .catch((err: any) => {
        const status = err.response === undefined ? 12023 : err.response.status;
        alert(status);
        return {
            data: {},
            status,
        };
    });
};