import { useState, ChangeEvent, FormEvent} from 'react';
import './App.css';

function App() {

 
  interface listReq {
    todoName: string;
  }

  // const [todoList, setTodoList] = useState<todoList[]>([]);
  const [list, setList] = useState<listReq[]>([])

  const addNewTodoList = (e: ChangeEvent<HTMLInputElement>): void => {
    const inputValue: listReq[] = [{ todoName: e.target.value }];

    if (typeof inputValue === "string") {
      setList({
        ...list,
        [e.target.name]: e.target.value
      })
    }
  }

  const addTodoAction = (e: FormEvent<HTMLFormElement>): void => {
    e.preventDefault();

  }


  return (
    <div className="App">
      <form onSubmit={addTodoAction}>
        <input
          name="todoName"
          type="text"
          placeholder="to do"
          onChange={addNewTodoList}
          required
        />

        <input
          type="submit"
        >

        </input>
      </form>
    </div>
  );
}

export default App;
