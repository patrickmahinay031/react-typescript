import React, { memo, ChangeEvent, FormEvent, useState } from 'react'

const TodoList = () => {

    interface todoInput {
        todoDescription: string;
    }

    const [todo, setTodo] = useState<todoInput[]>([])
    const [task, setTask] = useState<string>("");

    const onChange = (e: ChangeEvent<HTMLInputElement>): void => {
        const inputValue = e.target.value;
        if (typeof inputValue === "string") {
            setTask(inputValue)
        }
    }

    const onSubmit = (e: FormEvent<HTMLFormElement>): void => {
        e.preventDefault();
        addNewTodo(task);
    }

    const addNewTodo = (todoName: string): void => {
        const todoList: todoInput[] = [...todo, { todoDescription: todoName }];
        setTodo(todoList);
        setTask("");
    }

    const removeTodo = (id: number): void => {
        const todoList: todoInput[] = [...todo];
        todoList.splice(id);
        setTodo(todoList);
    }

    const todoCards = (item: todoInput, index: number): any => {
        return (
            <div key={index} className="bg-white rounded-md h-10 w-32 flex flex-row gap-2 justify-center items-center">
                <div className='text-xl font-semibold capitalize'>{item.todoDescription}</div>
                <div
                    className='bg-red-700 text-white w-4 rounded-full flex justify-center cursor-pointer'
                    onClick={() => removeTodo(index)}
                >
                    x
                </div>
            </div>
        )
    }

    return (
        <div className="flex flex-col gap-10 justify-center items-center bg-red-500 min-h-screen py-10">
            <div className=''>
                <form onSubmit={onSubmit} className="space-x-5">
                    <input
                        className='p-1 rounded'
                        name="todoName"
                        type="text"
                        placeholder="to do"
                        value={task}
                        onChange={onChange}
                        required
                    />
                    <input
                        className='bg-white text-gray-700 font-bold p-1 rounded'
                        type="submit"
                    >
                    </input>
                </form>
            </div>
            <div className='flex flex-row space-x-1'>
                {todo.length > 0 ? todo.map(todoCards) : null}
            </div>
        </div>
    )
}

export default memo(TodoList)